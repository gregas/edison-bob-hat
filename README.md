## edison-bob-hat
### Hat design for Intel Edison Breakout Board

This repo contains Kicad design files for a "hat" that connects to J17-J19 on Edision Mini Breakout Board. This can be used as staring point for custom hats.

Status: Not yet check or fabricated. Use at your own risk.
