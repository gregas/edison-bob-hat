EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Edison Breakout Board Hat"
Date "2016-08-10"
Rev "A"
Comp "SkyHigh Designs"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X14 P17
U 1 1 57ABF493
P 2550 5400
F 0 "P17" H 2550 6150 50  0000 C CNN
F 1 "CONN_01X14" V 2650 5400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14" H 2550 5400 50  0001 C CNN
F 3 "" H 2550 5400 50  0000 C CNN
	1    2550 5400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X14 P18
U 1 1 57ABFFE0
P 4900 5400
F 0 "P18" H 4900 6150 50  0000 C CNN
F 1 "CONN_01X14" V 5000 5400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14" H 4900 5400 50  0001 C CNN
F 3 "" H 4900 5400 50  0000 C CNN
	1    4900 5400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X14 P19
U 1 1 57AC017B
P 7050 5400
F 0 "P19" H 7050 6150 50  0000 C CNN
F 1 "CONN_01X14" V 7150 5400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14" H 7050 5400 50  0001 C CNN
F 3 "" H 7050 5400 50  0000 C CNN
	1    7050 5400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X14 P20
U 1 1 57AC020A
P 9400 5400
F 0 "P20" H 9400 6150 50  0000 C CNN
F 1 "CONN_01X14" V 9500 5400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x14" H 9400 5400 50  0001 C CNN
F 3 "" H 9400 5400 50  0000 C CNN
	1    9400 5400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
